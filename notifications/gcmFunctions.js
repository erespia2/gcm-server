module.exports.sendMessageFunction = function(receiver, namesender, senderId, idRoute, type, idur, res) {
    var gcm = require('node-gcm')
    , sender = new gcm.Sender('AIzaSyClBprsTLkbUoiFhHUtSLHpWt0GHc5vGF0')
    , User = require('../models/user')
    , Route = require('../models/route');  
    User.findOne({_id: receiver}, function(error, user) {
      if ((user !== undefined) && (user !== null)){
        if ((user.idgcm !== null) && (user.idgcm !== undefined) && (user.idgcm !== "")){
          Route.findOne({_id: idRoute}, function(error, route) {
            if ((route !== null) && (route !== undefined)) {
              var message;
              message = new gcm.Message;
              var registrationIds=[];
              console.log("Notificacion enviada de: "+senderId+"a: "+user.idgcm);
              registrationIds.push(user.idgcm);
              var msg = {};
              msg.idur = idur;
              msg.namesender = namesender;
              msg.idsender = senderId;
              msg.idreceiver = receiver;
              msg.idUser = user.idfb;
              msg.fromStr = route.fromstr;
              msg.toStr = route.tostr;
              msg.fromLat = route.fromlat;
              msg.toLat = route.tolat;
              msg.fromLng = route.fromlng;
              msg.toLng = route.tolng;
              msg.idRoute = idRoute;
              msg.time = route.time;
              msg.repeat = route.repeat;
              msg.day = route.day;
              msg.mondays = route.mondays;
              msg.tuesdays = route.tuesdays;
              msg.wednesdays = route.wednesdays;
              msg.thursdays = route.thursdays;
              msg.fridays = route.fridays;
              msg.saturdays = route.saturdays;
              msg.sundays = route.sundays;
              msg.money = route.pricemax;
              msg.telephone = route.telephone;
              msg.animal = route.animal;
              msg.people = route.onlywomen;
              msg.smoke = route.smoker;
              msg.love = route.love;
              msg.seats = route.places;
              msg.type = type;
              var jsonString = JSON.stringify(msg);
              message.addData('message', jsonString);
              sender.send(message, registrationIds, 4, function(error, result) {
                 if (error === null)
                    res.send("true");
                 else{
                    console.log(result);
                    res.send("false");
                 }
              });
            } else {
              res.send("false");
            }
          });
        } else{
          res.send("notRegistered");  
        }
      } else {
          res.send("false");
      }
    });
}

module.exports.sendMessageFunctionDelete = function(receiver, from, to, senderU, type, time, day) {
    var gcm = require('node-gcm')
    , sender = new gcm.Sender('AIzaSyClBprsTLkbUoiFhHUtSLHpWt0GHc5vGF0')
    , User = require('../models/user')
    , Route = require('../models/route');  
    User.findOne({_id: receiver}, function(error, user) {
      if ((user !== undefined) && (user !== null)){
        if ((user.idgcm !== null) && (user.idgcm !== undefined) && (user.idgcm !== "")){
          User.findOne({_id: senderU}, function(error, userS) {
            if ((userS !== null) && (userS !== undefined)){
              var message;
              message = new gcm.Message;
              var registrationIds=[];
              registrationIds.push(user.idgcm);
              var msg = {};
              msg.type = type;
              msg.time = time;
              msg.day = day;
              msg.from = from;
              msg.to = to;
              msg.idUser = user.idfb;
              msg.namesender = userS.name;
              var jsonString = JSON.stringify(msg);
              message.addData('message', jsonString);
              sender.send(message, registrationIds, 4, function(error, result) {
                 if (error !== null)
                    console.log(result);
              });
            }
          });
        }
      }
    });
}