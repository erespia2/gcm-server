module.exports = function(app){  

  var gcm = require('node-gcm')
    , sender = new gcm.Sender('AIzaSyClBprsTLkbUoiFhHUtSLHpWt0GHc5vGF0')
    , User = require('../models/user')
    , Route = require('../models/route');  
   
  register = function(req, res) {
    User.findOne({_id: req.body.idUser}, function(error, user) {
      if (req.body.idgcm !== undefined){
        user.idgcm=req.body.idgcm;
        user.save();
      }
      res.send("true");  
    });
  };
   
  unregister = function(req, res) {
    User.findOne({_id: req.body.idUser}, function(error, user) {
      user.idgcm=null;
      user.save();
      res.send("true");  
    });
  };
   
  sendMessage = function(req, res) {
    User.findOne({_id: req.body.receiver}, function(error, user) {
      if (user !== null){
        if ((user.idgcm !== null) && (user.idgcm !== undefined) && (user.idgcm !== "")){
          Route.findOne({_id: req.body.idRoute}, function(error, route) {
            if (route !== null) {
              var message;
              message = new gcm.Message;
              var registrationIds=[];
              registrationIds.push(user.idgcm);
              var msg = {};
              msg.namesender = req.body.namesender;
              msg.idsender = req.body.sender;
              msg.idreceiver = req.body.receiver;
              msg.idUser = user.idfb;
              msg.fromStr = route.fromstr;
              msg.toStr = route.tostr;
              msg.fromLat = route.fromlat;
              msg.toLat = route.tolat;
              msg.fromLng = route.fromlng;
              msg.toLng = route.tolng;
              msg.idRoute = req.body.idRoute;
              msg.time = route.time;
              msg.repeat = route.repeat;
              msg.day = route.day;
              msg.mondays = route.mondays;
              msg.tuesdays = route.tuesdays;
              msg.wednesdays = route.wednesdays;
              msg.thursdays = route.thursdays;
              msg.fridays = route.fridays;
              msg.saturdays = route.saturdays;
              msg.sundays = route.sundays;
              msg.money = route.pricemax;
              msg.telephone = route.telephone;
              msg.animal = route.animal;
              msg.people = route.onlywomen;
              msg.smoke = route.smoker;
              msg.love = route.love;
              msg.seats = route.places;
              msg.type = req.body.type;
              var jsonString = JSON.stringify(msg);
              message.addData('message', jsonString);
              sender.send(message, registrationIds, 4, function(error, result) {
                 if (error === null)
                    res.send("true");
                 else{
                    console.log(result);
                    res.send("false");
                 }
              });
            } else {
              res.send("false");
            }
          });
        } else{
          res.send("notRegistered");  
        }
      } else {
          res.send("false");
      }
    });
    
  };

  app.post('/gcm/register',   register);
  app.post('/gcm/unregister', unregister);
  app.post('/gcm/send',       sendMessage);

} 