// Friends user Class
var mongoose = require('mongoose'),  
    Schema = mongoose.Schema;  
  
var friendsUserSchema = new Schema({  
	iduser: String,
    idfriend: String
});  
  
//Export the schema  
module.exports = mongoose.model('FriendsUser', friendsUserSchema); 