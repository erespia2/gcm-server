// Route Class
var mongoose = require('mongoose'),  
    Schema = mongoose.Schema;  
  
var routeSchema = new Schema({  
    iduser: String,
	fromstr: String,
	tostr: String,
	fromlat: Number,
	tolat: Number,
	fromlng: Number,
	tolng: Number,
	places: Number,
	idcar: String,
	pricemin: Number,
	pricemax: Number,
	smoker: Boolean,
	animal: Boolean,
	onlywomen: Boolean,
	love: Boolean,
	time: String,
	date: Date,
	mondays: Boolean,
	tuesdays:Boolean,
	wednesdays: Boolean,
	thursdays: Boolean,
	fridays: Boolean,
	saturdays: Boolean,
	sundays: Boolean,
	day: String,
	repeat: Boolean,
	telephone: String,
	email: String,
	rating: { type: Boolean, default: false }, // Personal RATING Dynamic
	deleted: { type: Boolean, default: false }
});  
  
//Export the schema  
module.exports = mongoose.model('Route', routeSchema);  