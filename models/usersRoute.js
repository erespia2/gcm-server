// Users share a route Class
var mongoose = require('mongoose'),  
    Schema = mongoose.Schema;  
  
var usersRouteSchema = new Schema({  
    idroute: String,
    iduser: String,
    state: String, // A: Aceptado, P: Pendiente
    type: String, // P: Pasajero, D: Conductor
    rating: { type: Number, default: null },
    myratingdriver: { type: Number, default: null },
    votes: [Number],
    deleted: { type: Boolean, default: false }
});  
  
//Export the schema  
module.exports = mongoose.model('UsersRoute', usersRouteSchema); 