// User Class
var mongoose = require('mongoose'),  
    Schema = mongoose.Schema;  
  
var userSchema = new Schema({  
    name: String,
    idfb: String,
    idtwitter: String,
    idgplus: String,
    idgcm: String,
    rating: Number
});  
  
//Export the schema  
module.exports = mongoose.model('User', userSchema); 