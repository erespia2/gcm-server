module.exports = function(app){  

    var UsersRoute = require('../models/usersRoute');  
    var Route = require('../models/route'); 
    var User = require('../models/user'); 
    var Gcm = require('../notifications/gcmFunctions');

    //Create a new UsersRoute and save it  
    newUsersRoute = function(req, res){  
        var usersRoute = new UsersRoute({
                               idroute: req.body.idroute, 
                               iduser: req.body.iduser, 
                               state: req.body.state, 
                               type: req.body.type,
                               rating: req.body.rating,
                               myratingdriver: req.body.myratingdriver});  
        usersRoute.save();  
        res.send("true");  
    };  

    //find number usersRoutes as passanger
    countUsersRoutesByUserAsPassanger = function(req, res){  
        UsersRoute.find({$and: [{type: "P"}, {deleted: false}, {iduser: req.params.iduser}]}).count(function(err, count) {  
            res.json({'total':count});  
        });  
    };
  
    //find all usersRoutes 
    listUsersRoutes = function(req, res){  
        UsersRoute.find({}, function(err, usersRoutes) {  
            res.send(usersRoutes);  
        });  
    };  
  
    //find UsersRoute by idRoute and idUser 
    findUsersRouteByUserAndRoute = function(req, res) {  
        UsersRoute.findOne({$and: [{idroute: req.params.idroute}, {iduser: req.params.iduser}, {deleted: false}]}, function(error, userRoute) {  
            res.send(userRoute);  
        });
    };

    //find UsersRoute by idRoute and idUser 
    findUsersRouteByUserAndRouteDeleted = function(req, res) {  
        UsersRoute.findOne({_id: req.params.id}, function(error, userRoute) {
            if (userRoute !== null){
                Route.findOne({_id:userRoute.idroute}, function(error, route){
                    if (route !== null){
                        if (route.deleted === true)
                            userRoute.type="deletedRoute";
                        res.send(userRoute);
                    }
                });
            } else {
                res.send(null);
            } 
        });
    };

    //find UsersRoute by idRoute 
    findUsersRouteByRoute = function(req, res) {  
        Route.findOne({$and: [{_id: req.params.idroute}, {deleted: false}]}, function(error, route) {
             if (route !== null){
                 var dateNow = new Date();
                 dateNow.setHours(dateNow.getHours()+1);
                 var dateRoute= new Date(route.date);
                 if (dateNow > dateRoute){
                    UsersRoute.find({$and: [{idroute: req.params.idroute}, {deleted: false}, {state: "A"}]}, function(error, usersRoutes) {  
                        res.send(usersRoutes);  
                    });
                 } else {
                    UsersRoute.find({$and: [{idroute: req.params.idroute}, {deleted: false}]}, function(error, usersRoutes) {  
                        res.send(usersRoutes);  
                    });
                 }
            } else {
                res.send(null);
            }
         });
    };

    //find UsersRoute by idRoute 
    findUsersRouteAcceptByRoute = function(req, res) {  
        UsersRoute.find({$and: [{idroute: req.params.idroute}, {state: "A"}, {deleted: false}]}, function(error, usersRoutes) {  
            res.send(usersRoutes);  
        });
    };

    //find Routes as Passanger by idUser
    findRoutesAsPassangerByUser = function(req, res) { 
        UsersRoute.find({$and: [{type: "P"}, {iduser: req.params.iduser}, {deleted: false}]}, function(error, usersRoutes) {
            if (usersRoutes !== null){
                var idroutes=[];
                usersRoutes.forEach(function(route) {
                    idroutes.push(route.idroute);
                });

                var nowNoGMT = new Date();
                var now=new Date(nowNoGMT.valueOf() - (nowNoGMT.getTimezoneOffset() * 60000));
                var timezone=0;

                try{
                    strTimezone=req.params.timezone;
                    strHourTZ=strTimezone[1]+strTimezone[2];
                    timezone=strHourTZ;
                    if(strTimezone[0]==="-")
                      timezone=timezone*-1;
                    else
                      timezone=timezone*1;
                }catch(e){
                    console.log("Fallo al TIMEZONE: "+req.params.timezone);
                }

                now.setHours(now.getHours()+timezone);

                Route.find({$and: [{date: {$gt: now}},
                                   {_id: {$in: idroutes}},
                                   {deleted: false}
                                  ]}).sort({date:1}).limit(30).exec(function(err, routesN) {  
                    Route.find({$and: [{date: {$lte: now}},
                                       {_id: {$in: idroutes}},
                                       {deleted: false}
                                      ]}).sort({date:-1}).limit(20).exec(function(err, routesO) {
                      if (routesO === null)
                            res.send(routesN);
                      else {
                          var wait = routesO.length;
                          if (wait <= 0){
                            var routes = routesN.concat(routesO);
                            res.send(routes);  
                          } else {
                            routesO.forEach(function(route) {
                              UsersRoute.findOne({$and: [{idroute: route._id}, {deleted: false}, {iduser:req.params.iduser}, {state: "A"}]}, function(error, uRoute) {
                                route.rating = true;
                                if ((uRoute === null) || (uRoute.myratingdriver === null))
                                    route.rating = false;
                                route.save();
                                wait--;
                                if (wait <= 0){
                                  var routes = routesN.concat(routesO);
                                  res.send(routes);  
                                }
                              });
                            });
                          }  
                       }
                    });
                });
            } else {
                res.send(null);
            }
        });
    };

    //update a usersRoute
    updateUsersRoute = function(req, res){
        UsersRoute.findOne({$and: [{idroute: req.params.idroute}, {iduser: req.params.iduser}, {deleted: false}]}, function(error, usersRoute) { 
            if (usersRoute !== null){
                if (req.body.idroute !== undefined)
                  usersRoute.idroute=req.body.idroute;
                if (req.body.iduser !== undefined)
                  usersRoute.iduser=req.body.iduser; 
                if (req.body.state !== undefined)
                  usersRoute.state=req.body.state; 
                if (req.body.type !== undefined)
                  usersRoute.type=req.body.type;
                if (req.body.rating !== undefined)
                  usersRoute.rating=req.body.rating;
                if (req.body.myratingdriver !== undefined)
                  usersRoute.myratingdriver=req.body.myratingdriver;
                usersRoute.save();
                res.send("true");
            } else {
                res.send("false");
            } 
        });
    };

    //update a usersRoute and notification
    updateUsersRouteSendNotification = function(req, res){
        UsersRoute.findOne({$and: [{idroute: req.params.idroute}, {iduser: req.params.iduser}, {deleted: false}]}, function(error, usersRoute) { 
            if (usersRoute !== null){
                if (req.body.idroute !== undefined)
                  usersRoute.idroute=req.body.idroute;
                if (req.body.iduser !== undefined)
                  usersRoute.iduser=req.body.iduser; 
                if (req.body.state !== undefined)
                  usersRoute.state=req.body.state; 
                if (req.body.type !== undefined)
                  usersRoute.type=req.body.type;
                if (req.body.rating !== undefined)
                  usersRoute.rating=req.body.rating;
                if (req.body.myratingdriver !== undefined)
                  usersRoute.myratingdriver=req.body.myratingdriver;
                usersRoute.save();
                Gcm.sendMessageFunction(req.body.receiver, req.body.namesender, req.body.sender, req.body.idRoute, req.body.typeNotification, usersRoute._id, res);
            } else {
                res.send("false");
            } 
        });
    };

    // updates places of route and notification
    newUsersRouteSendNotification = function(req, res){
        Route.findOne({_id: req.body.idroute}, function(error, route) {
          if (route !== null){
              if (req.body.places !== undefined){
                 if ((req.body.places < 0) && (route.places < 1)){
                    res.send("notPlaces");
                 } else {
                    var usersRoute = new UsersRoute({
                                   idroute: req.body.idroute, 
                                   iduser: req.body.sender, 
                                   state: "P", 
                                   type: "P",
                                   deleted: false});  
                    usersRoute.save(); 
                    route.places=route.places+req.body.places;
                    route.save();
                    Gcm.sendMessageFunction(req.body.receiver, req.body.namesender, req.body.sender, req.body.idroute, req.body.typeNotification, usersRoute._id, res);
                 }
              } else {
                 res.send("false"); 
              }
          } else {
            res.send("false");
          }
        });
    };

    star2rating = function(rating, type){
        if (rating === 1){
            if (type === "driver")
                return -1;
            else if (type === "passanger")
                return -3;
        } else if (rating === 2){
            if (type === "driver")
                return 0.2;
            else if (type === "passanger")
                return -1;
        } else if (rating === 3){
            if (type === "driver")
                return 1;
            else if (type === "passanger")
                return 0.1;
        } else if (rating === 4){
            if (type === "driver")
                return 2;
            else if (type === "passanger")
                return 1;
        } else if (rating === 5){
            if (type === "driver")
                return 3;
            else if (type === "passanger")
                return 2;
        }
        return 0;
    }

    //update a usersRoute rating driver
    updateUsersRouteRatingDriver = function(req, res){
        UsersRoute.findOne({$and: [{idroute: req.params.idroute}, {type: 'D'}, {deleted: false}]}, function(error, usersRoute) {
            if (usersRoute !== null){
                if (req.body.rating !== undefined){
                    var ratingConverse = star2rating(req.body.rating, "driver");
                    usersRoute.votes.push(ratingConverse);
                    var media=0;
                    usersRoute.votes.forEach(function(vote) {
                        media += vote;
                    });
                    usersRoute.rating = media/usersRoute.votes.length;
                    usersRoute.save();
                    UsersRoute.findOne({$and: [{idroute: req.params.idroute}, {iduser: req.params.iduser}, {deleted: false}]}, function(error, usersRoute) { 
                        if (req.body.rating !== undefined){
                            usersRoute.myratingdriver=ratingConverse;
                            usersRoute.save();
                            res.send("true");
                        } else {
                            res.send("false");
                        }
                    });
                } else {
                    res.send("false");
                }
            } else {
                res.send("false");
            }
        });
    };

    //update a usersRoute
    updateUsersRouteRatingPassanger = function(req, res){
        User.findOne({idfb: req.params.iduserfb}, function(error, user) {
            if (user !== null){
                UsersRoute.findOne({$and: [{idroute: req.params.idroute}, {iduser: user._id}, {deleted: false}]}, function(error, usersRoute) { 
                    if (req.body.rating !== undefined){
                        usersRoute.rating=star2rating(req.body.rating, "passanger");
                        usersRoute.save();
                        res.send("true"); 
                    } else {
                        res.send("false");
                    }
                });
            } else {
                res.send("false");
            }
        });
    };

    //delete UsersRoute by iduser and idroute  
    deleteUsersRoute = function(req, res) {  
        UsersRoute.findOne({$and: [{idroute: req.params.idroute}, {iduser: req.params.iduser}]}, function(error, usersRoute) {  
            if (usersRoute !== null){
                usersRoute.deleted = true;
                usersRoute.save();
                res.send("true");
            } else {
                res.send("false");
            }  
        });
    };

    //Link usersRoutes and functions  
    app.post('/usersRoute', newUsersRoute);  
    app.post('/usersRoutesSubPlaceSendNotification', newUsersRouteSendNotification);
    app.get('/usersRoutes', listUsersRoutes);  
    app.get('/usersRoutes/:idroute/:iduser', findUsersRouteByUserAndRoute);
    app.get('/usersRoutesDeleted/:id', findUsersRouteByUserAndRouteDeleted);
    app.get('/usersRoutes/:idroute', findUsersRouteByRoute);
    app.get('/usersRoutesAccept/:idroute', findUsersRouteAcceptByRoute);
    app.get('/routesAsPassanger/:iduser/:timezone', findRoutesAsPassangerByUser);    
    app.get('/nusersRoutes/:iduser', countUsersRoutesByUserAsPassanger);
    app.put('/usersRoutes/:idroute/:iduser', updateUsersRoute);
    app.put('/usersRoutesSendNotification/:idroute/:iduser', updateUsersRouteSendNotification);
    app.put('/usersRoutes/:idroute/:iduser', updateUsersRoute);
    app.put('/usersRoutesUpdateRatingDriver/:idroute/:iduser', updateUsersRouteRatingDriver);
    app.put('/usersRoutesUpdateRatingPassanger/:idroute/:iduserfb', updateUsersRouteRatingPassanger);
    app.delete('/usersRoutes/:idroute/:iduser', deleteUsersRoute);

} 