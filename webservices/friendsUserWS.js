module.exports = function(app){  

    var FriendsUser = require('../models/friendsUser');  
    //Create a new Route and save it  
    newFriendsUser = function(req, res){  
        FriendsUser.findOne({$and: [{idfriend: req.body.idfriend}, {iduser: req.body.iduser}]}, function(err, friend) {  
            if (friend === null){
                var friendUser = new FriendsUser({
                               iduser: req.body.iduser, 
                               idfriend: req.body.idfriend});  
                friendUser.save(); 
                res.send("true"); 
            } else {
                res.send("false"); 
            }
        });
    };  
  
    //find all friends user 
    listFriendsByUser = function(req, res){  
        FriendsUser.find({iduser: req.params.iduser}, function(err, friends) {  
            res.send(friends);  
        });  
    };  

    //delete friend by idFriend and idUser  
    deleteFriendsUser = function(req, res) {  
        FriendsUser.findOne({$and: [{idfriend: req.params.idfriend}, {iduser: req.params.iduser}]}, function(error, friend) {  
            friend.remove();
            res.send("true");  
        });
    };

    //Link routes and functions  
    app.post('/friendsuser', newFriendsUser);  
    app.get('/friendsuser/:iduser', listFriendsByUser);  
    app.delete('/friendsuser/:iduser/:idfriend', deleteFriendsUser);

} 