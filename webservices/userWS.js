module.exports = function(app){  

    var User = require('../models/user');
    var UsersRoute = require('../models/usersRoute');  
    var Route = require('../models/route'); 
    //Create a new User and save it  
    newUser = function(req, res){ 
        User.findOne({idfb: req.body.idfb}, function(error, user) {
            if (user === null) {
              var user = new User({  name: req.body.name, 
                               idfb: req.body.idfb, 
                               idtwitter: req.body.idtwitter, 
                               idgplus: req.body.idgplus,
                               idgcm: req.body.idgcm});  
              user.save();
            }
            res.send(user);  
        });
    };  
  
    //find all users 
    listUsers = function(req, res){  
        User.find({}, function(err, users) {
            res.send(users);  
        });  
    };  
  
    //find user by id  
    findUser = function(req, res) {  
        User.findOne({_id: req.params.id}, function(error, user) {
            res.send(user);  
        });
    };

    //update a user
    updateUser = function(req, res){
        User.findOne({_id: req.params.id}, function(error, user) { 
            if (user !== null){
              if (req.body.idfb !== undefined)
                user.idfb=req.body.idfb;
              if (req.body.idtwitter !== undefined)
                user.idtwitter=req.body.idtwitter; 
              if (req.body.idgplus !== undefined)
                user.idgplus=req.body.idgplus; 
              if (req.body.name !== undefined)
                user.name=req.body.name;
              if (req.body.idgcm !== undefined)
                user.idgcm=req.body.idgcm;
              user.save();
              res.send("true");  
            } else {
              res.send("false");
            }
        });
    };

    //delete user by id  
    deleteUser = function(req, res) {  
        User.findOne({_id: req.params.id}, function(error, user) {  
            user.remove();
            res.setEncoding('utf8');
            res.send("true");  
        });
    };

    calculateRatingStatic = function (idUser, ret, res){
      UsersRoute.find({iduser: idUser}, function(err, usersRoute) {
          if (usersRoute !== null){
            var ratingD = 0;
            var ratingP = 0;
            var numberRatings = 0;
            usersRoute.forEach(function(uRoute) {
                if (uRoute.type === "D"){
                  if (uRoute.rating !== null){
                    ratingD += uRoute.rating;
                    numberRatings += uRoute.votes.length;
                  }
                } else if (uRoute.type === "P"){
                  if (uRoute.rating !== null){
                    ratingP += uRoute.rating;
                    numberRatings += 1;
                  }
                }
            });
            ratingP *= 0.85;
            var rating = ((ratingP+ratingD)/2);
            /*if (numberRatings =< 10)
              rating = 0;
            else if ((numberRatings > 10) && (numberRatings <= 20)
              rating = ((ratingP+ratingD)/2) * 0.5;
            else if ((numberRatings > 20) && (numberRatings <= 30)
              rating = ((ratingP+ratingD)/2) * 0.75;
            else if (numberRatings > 30)
              rating = ((ratingP+ratingD)/2);*/
            ret.rating += rating;
            res.json(ret);  
          } else {
            res.send(null);
          }
      });
    };

    //find info of user 
    infoUserBar = function(req, res) {  
        UsersRoute.find({$and: [{type: "P"}, {iduser: req.params.iduser}, {deleted: false}]}).count(function(err, count) {  
              var ret = {};
              ret.nRoutesAsPassanger = count;
              Route.find({$and: [{iduser: req.params.iduser}, {deleted: false}]}).count(function(err, count) { 
                    ret.nRoutesAsDriver = count;
                    ret.rating = 0;
                    calculateRatingStatic(req.params.iduser, ret, res);
              });
        });
    };

    //find info of user 
    infoUserBarDynamicRating = function(req, res) {  
        UsersRoute.find({$and: [{type: "P"}, {iduser: req.params.iduser}, {deleted: false}]}).count(function(err, count) {  
              var ret = {};
              ret.nRoutesAsPassanger = count;
              Route.find({$and: [{iduser: req.params.iduser}, {deleted: false}]}).count(function(err, count) { 
                ret.nRoutesAsDriver = count;
                ret.rating = 0;
                if (req.params.degree == 1){
                   ret.rating += 7;
                } else if (req.params.degree == 2){
                   ret.rating += 6;
                } else {
                   ret.rating += 4;
                }
                calculateRatingStatic(req.params.iduser, ret, res); 
              });       
        });
    };

    //Link users and functions  
    app.post('/user', newUser);  
    app.get('/users', listUsers);  
    app.get('/user/:id', findUser);
    app.get('/infoUserBar/:iduser', infoUserBar);
    app.get('/infoUserBar/:iduser/:degree', infoUserBarDynamicRating); 
    app.put('/user/:id', updateUser);
    app.delete('/user/:id', deleteUser);

} 