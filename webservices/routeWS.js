module.exports = function(app){  

    var Route = require('../models/route');  
    var Gcm = require('../notifications/gcmFunctions');
    var UsersRoute = require('../models/usersRoute');  
    //Create a new Route and save it  
    newRoute = function(req, res){  
        var dateBig = req.body.day.split("/");
        var dateSmall = req.body.time.split(":");
        var dateFormat = new Date(dateBig[2], dateBig[1]-1, dateBig[0], dateSmall[0], dateSmall[1], 0, 0);
        Route.findOne({$and: [{fromlat: req.body.fromlat}, {iduser: req.body.iduser}, {tolat: req.body.tolat}, {fromlng: req.body.fromlng}, {tolng: req.body.tolng}, {date: dateFormat}, {deleted: false}]}, function(error, eRoute) {
          if ((eRoute === null) || (eRoute === undefined)){
            var route = new Route({
                                   iduser: req.body.iduser, 
                                   fromstr: req.body.fromstr, 
                                   tostr: req.body.tostr, 
                                   fromlat: req.body.fromlat, 
                                   tolat: req.body.tolat, 
                                   fromlng: req.body.fromlng, 
                                   tolng: req.body.tolng, 
                                   places: req.body.places, 
                                   idcar: req.body.idcar, 
                                   pricemin: req.body.pricemin,
                                   pricemax: req.body.pricemax, 
                                   smoker: req.body.smoker, 
                                   animal: req.body.animal, 
                                   onlywomen: req.body.onlywomen, 
                                   love: req.body.love, 
                                   time: req.body.time, 
                                   date: dateFormat,
                                   mondays: req.body.mondays, 
                                   tuesdays: req.body.tuesdays, 
                                   wednesdays: req.body.wednesdays, 
                                   thursdays: req.body.thursdays, 
                                   fridays: req.body.fridays, 
                                   saturdays: req.body.saturdays, 
                                   sundays: req.body.sundays, 
                                   day: req.body.day, 
                                   repeat: req.body.repeat, 
                                   telephone: req.body.telephone, 
                                   email: req.body.email,
                                   deleted: false});  
            route.save(); 
            var usersRoute = new UsersRoute({
                                   idroute: route._id, 
                                   iduser: req.body.iduser, 
                                   state: req.body.state, 
                                   type: req.body.type,
                                   myratingdriver: req.body.myratingdriver});  
            usersRoute.save(); 
            res.send("true");
          } else {
            res.send("exist");
          }
      });
    };  

    //find number user's routes 
    countRoutesByUser = function(req, res){  
        Route.find({$and: [{iduser: req.params.iduser}, {deleted: false}]}).count(function(err, count) {  
            res.json({'total':count});  
        });  
    };
  
    //find all user's routes 
    listRoutesByUser = function(req, res){  
        
        var nowNoGMT = new Date();
        var now=new Date(nowNoGMT.valueOf() - (nowNoGMT.getTimezoneOffset() * 60000));
        var timezone=0;

        try{
            strTimezone=req.params.timezone;
            strHourTZ=strTimezone[1]+strTimezone[2];
            timezone=strHourTZ;
            if(strTimezone[0]==="-")
              timezone=timezone*-1;
            else
              timezone=timezone*1;
        }catch(e){
            console.log("Fallo al TIMEZONE: "+req.params.timezone);
        }

        Route.find({$and: [{date: {$gt: now}},
                           {iduser: req.params.iduser},
                           {deleted: false}
                          ]}).sort({date:1}).limit(30).exec(function(err, routesN) {
            Route.find({$and: [{date: {$lte: now}},
                               {iduser: req.params.iduser},
                               {deleted: false}
                              ]}).sort({date:-1}).limit(20).exec(function(err, routesO) {
                  var wait = routesO.length;
                  if (wait <= 0){
                    var routes = routesN.concat(routesO);
                    res.send(routes); 
                  } else {
                    var index = 0;
                    routesO.forEach(function(route) {
                      UsersRoute.find({$and: [{idroute: route._id}, {deleted: false}, {type:"P"}, {state: "A"}]}, function(error, usersRoute) {
                        route.rating = true;
                        usersRoute.forEach(function(uRoute) {
                          if (uRoute.rating === null){
                            route.rating = false;
                          }
                          route.save();
                        });
                        if (usersRoute.length < 1)
                            routesO.splice(index, 1);
                        index++;
                        wait--;
                        if (wait <= 0){
                          var routes = routesN.concat(routesO);
                          res.send(routes);  
                        } 
                      });
                    });
                  }
            });
        });  
    };  
  
    //find route by id  
    findRoute = function(req, res) {  
        Route.findOne({_id: req.params.id}, function(error, route) {  
            res.send(route);  
        });
    };

    //find routes in area
    findRoutesInArea = function(req, res){  
        var now = new Date();
        now.setHours(now.getHours()+1);
        Route.find({$and: [{date: {$gt: now}}, 
                           {places: {$gt: 0}},
                           {deleted: false},
                           {iduser: {$ne: req.params.iduser}}, 
                           {fromlat: {$gt: req.params.fromlatmin, $lt: req.params.fromlatmax}}, 
                           {fromlng: {$gt: req.params.fromlngmin, $lt: req.params.fromlngmax}}, 
                           {tolat: {$gt: req.params.tolatmin, $lt: req.params.tolatmax}}, 
                           {tolng: {$gt: req.params.tolngmin, $lt: req.params.tolngmax}}]}).sort({date:1}).limit(50).exec(function(err, routes) { 
            res.send(routes);  
        });  
    };

    //update a route
    updateRoute = function(req, res){
        Route.findOne({_id: req.params.id}, function(error, route) { 
            if (req.body.iduser !== undefined)
              route.iduser=req.body.iduser; 
            if (req.body.fromstr !== undefined)
              route.fromstr=req.body.fromstr;
            if (req.body.tostr !== undefined) 
              route.tostr=req.body.tostr; 
            if (req.body.fromlat !== undefined)
              route.fromlat=req.body.fromlat;
            if (req.body.tolat !== undefined) 
              route.tolat=req.body.tolat;
            if (req.body.fromlng !== undefined)
              route.fromlng=req.body.fromlng;
            if (req.body.tolng !== undefined) 
               route.tolng=req.body.tolng; 
            if (req.body.places !== undefined)
               route.places=req.body.places; 
            if (req.body.idcar !== undefined)
               route.idcar=req.body.idcar; 
            if (req.body.pricemin !== undefined)
               route.pricemin=req.body.pricemin;
            if (req.body.pricemax !== undefined)
               route.pricemax=req.body.pricemax;
            if (req.body.smoker !== undefined) 
               route.smoker=req.body.smoker; 
            if (req.body.animal !== undefined)
               route.animal=req.body.animal; 
            if (req.body.onlywomen !== undefined)
               route.onlywomen=req.body.onlywomen; 
            if (req.body.love !== undefined)
               route.love=req.body.love; 
            if (req.body.time !== undefined)
               route.time=req.body.time; 
            if (req.body.mondays !== undefined)
               route.mondays=req.body.mondays;
            if (req.body.tuesdays !== undefined) 
               route.tuesdays=req.body.tuesdays;
            if (req.body.wednesdays !== undefined) 
               route.wednesdays=req.body.wednesdays; 
            if (req.body.thursdays !== undefined)
               route.thursdays=req.body.thursdays; 
            if (req.body.fridays !== undefined)
               route.fridays=req.body.fridays;
            if (req.body.saturdays !== undefined) 
               route.saturdays=req.body.saturdays;
            if (req.body.sundays !== undefined) 
               route.sundays=req.body.sundays; 
            if (req.body.day !== undefined)
               route.day=req.body.day; 
            if (req.body.repeat !== undefined)
               route.repeat=req.body.repeat; 
            if (req.body.telephone !== undefined)
               route.telephone=req.body.telephone; 
            if (req.body.email !== undefined)
               route.email=req.body.email;
            if ((req.body.time !== undefined) && (req.body.day !== undefined)){
               var dateBig = req.body.day.split("/");
               var dateSmall = req.body.time.split(":");
               var dateFormat = new Date(dateBig[2], dateBig[1]-1, dateBig[0], dateSmall[0], dateSmall[1], 0, 0);
               route.date = dateFormat;
            }
            route.save();
            res.send("true");
        });
    };

    // updates places of route
    updateRoutePlace = function(req, res){
        Route.findOne({_id: req.params.id}, function(error, route) {
            if (req.body.places !== undefined){
               if ((req.body.places < 0) && (route.places < 1)){
                 res.send("notPlaces");
               } else {
                 route.places=route.places+req.body.places;
                 route.save();
                 res.send("true");  
               }
            } else {
               res.send("false"); 
            }
        });
    };

    // updates places of route and notification
    updateRoutePlaceSendNotification = function(req, res){
        UsersRoute.findOne({$and: [{idroute: req.params.idroute}, {iduser: req.params.iduser}, {deleted: false}]}, function(error, usersRoute) {  
            usersRoute.deleted = true;
            usersRoute.save();
            Route.findOne({_id: req.params.idroute}, function(error, route) {
              if (req.body.places !== undefined){
                 if ((req.body.places < 0) && (route.places < 1)){
                    res.send("notPlaces");
                 } else {
                    route.places=route.places+req.body.places;
                    route.save();
                    Gcm.sendMessageFunction(req.body.receiver, req.body.namesender, req.body.sender, req.body.idRoute, req.body.typeNotification, usersRoute._id, res);
                 }
              } else {
                 res.send("false"); 
              }
            });
        });
    };

    //delete route by id  
    deleteRoute = function(req, res) {  
        Route.findOne({_id: req.params.id}, function(error, route) {  
            route.remove();
            res.send("true");  
        });
    };

    //delete route by id  
    deleteRouteAndSubRating = function(req, res) {  

        var rating = [];

        Route.findOne({_id: req.params.id}, function(error, route) {  
            if (route !== null){
              UsersRoute.find({$and: [{idroute: req.params.id}, {deleted: false}]}, function(error, usersRoute) {  
                  usersRoute.forEach(function(uRoute) {
                    if ((uRoute.type === "P") && (uRoute.state === "A")){
                      var date = new Date();
                      date.setHours(date.getHours()+1);
                      var ratingVeryPoor = new Date(route.date);
                      var ratingPoor = new Date(route.date);
                      ratingVeryPoor.setDate(route.date.getDate()-3);
                      ratingPoor.setDate(route.date.getDate()-8);
                      if (date > ratingVeryPoor){
                        rating.push(-4);
                      } else if (date > ratingPoor){
                        rating.push(-3.5);
                      } else {
                        rating.push(-3);
                      }
                    }
                    uRoute.deleted=true;
                    uRoute.save();
                    if (uRoute.type === "P") {
                       Gcm.sendMessageFunctionDelete(uRoute.iduser, route.fromstr, route.tostr, route.iduser, "Nd2pOFF", route.time, route.day);
                    }
                  });
                  UsersRoute.findOne({$and: [{idroute: req.params.id}, {type: 'D'}]}, function(error, usersRoute) {
                    if (usersRoute !== null){
                      rating.forEach(function(rat) {
                        usersRoute.votes.push(rat);
                      });
                      var media=0;
                      usersRoute.votes.forEach(function(vote) {
                          media += vote;
                      });
                      if (usersRoute.votes.length > 0)
                        usersRoute.rating = media/usersRoute.votes.length;
                      usersRoute.save();
                      
                      route.deleted=true;
                      route.save();
                      res.send("true");
                    } else {
                      res.send("exist");
                    }
                });
              });
            } else {
              res.send("exist");
            }
          });
    };

    //Link routes and functions  
    app.post('/route', newRoute);  
    app.get('/routes/:iduser/:timezone', listRoutesByUser);  
    app.get('/nroutes/:iduser', countRoutesByUser);
    app.get('/route/:id', findRoute);
    app.get('/routesinarea/:iduser/:fromlatmin/:fromlatmax/:fromlngmin/:fromlngmax/:tolatmin/:tolatmax/:tolngmin/:tolngmax', findRoutesInArea); 
    app.put('/route/:id', updateRoute);
    app.delete('/deleteRouteAndNotify/:id', deleteRouteAndSubRating);
    app.put('/routePlaces/:id', updateRoutePlace);
    app.put('/routePlacesSendNotification/:idroute/:iduser', updateRoutePlaceSendNotification);
    app.delete('/route/:id', deleteRoute);

} 