//Load app dependencies  
var express = require('express'),  
  mongoose = require('mongoose'), 
  path = require('path'),  
  http = require('http'),
  util = require('util');

var app = express();  

var cronJob = require('cron').CronJob;
  
//Configure: bodyParser to parse JSON data  
//           methodOverride to implement custom HTTP methods  
//           router to crete custom routes  
app.configure(function(){  
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});  
  
app.configure('development', function(){  
  app.use(express.errorHandler());  
});  
  
//Sample routes are in a separate module, just for keep the code clean  
routes = require('./routes/router')(app);  
  
//Connect to the MongoDB test database  
mongoose.connect('mongodb://localhost/hireucarTest');  
  
//Start the server  
http.createServer(app).listen(8080);  

// CronJobs
new cronJob('00 00 02 * * *', function(){
    console.log("Lanzando función para limpiar los usuarios que estan pendientes en rutas caducadas.")
    var UsersRoute = require('./models/usersRoute');  
    var Route = require('./models/route');
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate()-1);
    Route.find({$and: [{date: {$gte: yesterday}},
                       {date: {$lte: Date.now()}},
                       {deleted: false}
                      ]}, function(err, routes) {
                              routes.forEach(function(route) {
                                  UsersRoute.find({$and: [{idroute: route._id}, {state: "P"}, {deleted: false}]}, function(error, usersRoutes) {  
                                      usersRoutes.forEach(function(uRoute) {
                                          console.log("Borrando userRoute que estaba PENDIENTE y cuya RUTA ya CADUCO. idRoute: "+route._id+" - idUsersRoute: "+uRoute._id);
                                          uRoute.deleted=true;
                                          uRoute.save();
                                      });  
                                  });
                                  UsersRoute.find({$and: [{idroute: route._id}, {type: "P"}, {deleted: false}]}, function(error, usersRoutes) {  
                                      if (usersRoutes.length === 0){
                                        console.log("Borrando RUTA por estar CADUCADA y no contar con PASAJEROS. idRoute: "+route._id);
                                        route.deleted = true;
                                        route.save();
                                      }  
                                  });
                              });
                          }
    );
    
}, null, true, null);

new cronJob(new Date(), function(){
    console.log("Lanzando función para limpiar los usuarios que estan pendientes en rutas caducadas AL INICIO DEL SERVIDOR")
    var UsersRoute = require('./models/usersRoute');  
    var Route = require('./models/route');
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate()-1);
    Route.find({$and: [{date: {$lte: Date.now()}},
                       {deleted: false}
                      ]}, function(err, routes) {
                              routes.forEach(function(route) {
                                  UsersRoute.find({$and: [{idroute: route._id}, {state: "P"}, {deleted: false}]}, function(error, usersRoutes) {  
                                      usersRoutes.forEach(function(uRoute) {
                                          console.log("Borrando userRoute que estaba PENDIENTE y cuya RUTA ya CADUCO. idRoute: "+route._id+" - idUsersRoute: "+uRoute._id);
                                          uRoute.deleted=true;
                                          uRoute.save();
                                      });  
                                  });
                                  UsersRoute.find({$and: [{idroute: route._id}, {type: "P"}, {deleted: false}]}, function(error, usersRoutes) {  
                                      if (usersRoutes.length === 0){
                                        console.log("Borrando RUTA por estar CADUCADA y no contar con PASAJEROS. idRoute: "+route._id);
                                        route.deleted = true;
                                        route.save();
                                      }  
                                  });
                              });
                          }
    );
    
}, null, true, null);
