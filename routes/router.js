//App routes  
module.exports = function(app){  
  
    require('../webservices/routeWS')(app); 
    require('../webservices/userWS')(app); 
    require('../webservices/usersRouteWS')(app);
    require('../webservices/friendsUserWS')(app); 
    require('../notifications/gcmWS')(app);

}  